#!/bin/bash
number=0
while [ $number -lt 10 ];
do
    let "number=number+1"
    for i in {1..10}
    do
        echo "$number * $i =" $(($number * $i))
    done
    echo
done