#!/bin/bash
contador=0
if ! [[ "$1" =~ ^[0-9]+$ ]];
then
    echo "Insertar números mayores que 0"
    exit 1 
else    
    while [ $contador -ne $1 ];
        do
        echo -n "*"
        if [ $1 -ne $contador ];
            then
                let "contador=contador+1"
        fi
    done
echo
fi