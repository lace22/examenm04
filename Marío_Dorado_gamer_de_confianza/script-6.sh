#!/bin/bash
suma=0
for n in {1..100}
do
    if [ `expr $n % 2 ` -eq 1 ];
    then
        ((suma=$suma+$n))
    fi
done
    echo $suma
    exit