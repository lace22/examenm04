#!/bin/bash
echo "Pon un número"
read num1
echo "Pon otro más"
read num2
echo "Uno más y ya me callo"
read num3
if ! [[ "$num1" =~ ^[0-9]+$ &&  "$num2" =~ ^[0-9]+$  &&  "$num3" =~ ^[0-9]+$ ]];
then
    echo
    echo "no pongas letras compañero, eso está feo..."
else
    if [ $num1 -eq $num2 ]
    then
        if [ $num1 -gt $num3 ]
        then
            echo
            echo "$num1 $num2"
            echo "$num3"
        else
            echo
            echo "$num3"
            echo "$num1 $num2"
        fi
    fi
    if [ $num1 -eq $num3 ]
    then
        if [ $num1 -gt $num2 ]
        then
            echo
            echo "$num1 $num3"
            echo "$num2"
        else
            echo
            echo "$num2"
            echo "$num1 $num3"
        fi
    fi
    if [ $num2 -eq $num3 ]
    then
        if [ $num2 -gt $num1 ]
        then
            echo
            echo "$num2 $num3"
            echo "$num1"
        else
            echo
            echo "$num1"
            echo "$num2 $num3"
        fi
    fi
    if ! [ $num1 -eq $num2 ]
    then
        echo
        echo -e "${num1}\n${num2}\n${num3}" | sort -nr
    fi
fi