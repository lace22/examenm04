#!/bin/bash
if [ $# -eq 2 ];
then
    length1=${#1}
    length2=${#2}
    echo $length1 $length2
    if [ $length1 -gt $length2 ];
    then
        echo $1 "tiene mas dígitos que" $2
    else
        echo $2 "tiene mas dígitos que" $1
    fi
else
    echo "No has pasado 2 argumentos. Mal jugado compañero."
fi