#!/bin/bash
archivo=~/backups
if [ -d "$archivo" ]; then
    echo "$archivo ya existe"
    else
    mkdir $archivo
    echo "$archivo se ha creado"
fi
d=`date +%Y_%m_%d`
cd ~/
echo "Backing Up"

tar --exclude=backups -Pczf ~/backups/copia_${d}.tar.gz ~/
echo -"copia_${d}.tar.gz,$d" >> ~/.backups/log
echo "Borrando todas las copias anteriores a 15 dias uwu"
find ~/backups -type f -mtime +15 -name '*tar.gz' -execdir rm -- '{}' \;
