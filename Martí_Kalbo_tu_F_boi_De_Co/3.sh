#!/bin/bash

if [ $# -eq 2 ];
then
    if [ "$1" -gt "$2" ]; 
    then
	    echo "$1 es MAYOR que $2"
    else
        echo "$1 es MENOR que $2"
    fi
else
    echo "Arguments invàlids"
fi