#! /bin/bash

#Roll a dice using bash
re='^[0-9]+$'
read -p "¿Cuantas caras tiene el dado? """  dado
if ! [[ $dado =~ $re ]] ; 
then
   	echo "No pongas letras, decimales ni negativos" >&2;
elif [ $dado -le 1 ]
then
	echo "Eso es un numero muy bajo prueba otro"
else
	echo "Ha salido un..."
	sleep 1
	echo $(($RANDOM % $dado))
fi