#!/bin/bash
e=$1
i=0
re='^[0-9]+$'
if ! [[ $1 =~ $re ]] ; then
   echo "no pongas letras  ni decimales" >&2; exit 1
fi
while [ $i -lt $e ]
do
    echo -n "*"
    i=$[$i+1]
done
echo ""