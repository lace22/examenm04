#!/bin/bash
if [ $# = 2 ];
then 
        if [ $1 -gt $2 ];
        then
                echo "$1 es más grande que $2"
        else 
                echo "$2 es más grande que $1"
        fi
else
        echo "Has puesto $# argumentos, y han de ser 2."
fi 