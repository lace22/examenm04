#!/bin/bash

if ! [[ "$1" =~ ^[0-9]+$ ]]; then
	echo "Sorry only positive numbers"
else
	perl -e "print '* ' x $1"
	echo
fi