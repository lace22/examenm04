#!/bin/bash
primero=0
segundo=0
if ! [[ "$1" =~ ^[0-9]+$ ]]
    then
        echo "Solo enteros positivos, camarada"&&exit 1
fi
if ! [[ "$2" =~ ^[0-9]+$ ]]
    then
        echo "Solo enteros positivos, camarada"&&exit 1
fi
if [ $# -eq 2 ]; 
then
    if [ $1 -lt $2 ]; 
    then
        ((primero=$1))
        ((segundo=$2-1))
    
    elif [ $2 -lt $1 ];then
        ((primero=$2))
        ((segundo=$1-1))
    
    else 
        echo "Números diferentes, camarada"&&exit 1
    fi
else
    echo "Dos argumentos, camarada"&&exit 1
fi        
suma=$primero
for i in seq `seq $primero $segundo`;
do 
    echo $suma
    let suma=$suma+1
done
exit 0