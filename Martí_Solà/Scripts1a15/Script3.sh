#!/bin/bash
if [ $# -eq 2 ];
then 
	if [ $1 -gt $2 ];
	then
		echo "$1 es mayor que $2"
	elif [ $2 -eq $1 ];
	then
		echo "Son el mismo número"
	else 
		echo "$2 es mayor que $1"
	fi
else
	echo "Solo 2 argumentos, no $#"
fi 
