#!/bin/bash
((count = 0))
if ! [[ "$1" =~ ^[0-9]+$ ]]
    then
        echo "Solo enteros positivos, camarada"&&exit 1
fi
for i in seq `seq $count $1`;
do 
    echo -n "*"
    let count=$count+1
done
echo ""
exit 0
