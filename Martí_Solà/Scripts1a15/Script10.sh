#!/bin/bash
fila=1
columna=1
contador=1
while [ $columna -lt 11 ]
do
    while [ $fila -lt 11 ]
    do
        if [ $contador -eq 11 ];
        then
            contador=1
            echo " "
        else
            ((num=$fila*$columna))
            echo -n "["$fila" * "$columna" = "$num"] "
            fila=$[$fila+1]
            contador=$[$contador+1]
        fi
    done
    echo " "
    columna=$[$columna+1]
    fila=1
done
echo "  "