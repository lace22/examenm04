#!/bin/bash
actual=1
contador=1
while [ $actual -lt 101 ]
do
    if [ $contador -eq 11 ];
    then
        contador=1

        echo " "
    else
        if [ $actual -lt 10 ]
        then
            echo -n " 0"$actual" "
            actual=$[$actual+1]
            contador=$[$contador+1]
        else
            echo -n " $actual "
            actual=$[$actual+1]
            contador=$[$contador+1]
        fi
    fi
done
echo "  "