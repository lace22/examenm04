input=$1
total_time=$((input * 60))
echo "$total_time"
minutes=$((total_time / 60))
seconds=$((total_time % 60))
echo "Cuenta atrás: "
while [ $total_time -gt 0 ]
do
    minutes=$((total_time / 60))
    seconds=$((total_time % 60))
    printf "\r $minutes min $seconds sec"
    total_time=$[$total_time-1]
    sleep 1;
    done
if [ $total_time -eq 0 ]
then
clear
echo "Fin de la cuenta atrás"
fi