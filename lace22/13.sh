# Fer un script que demani tres nombres i detecti si s'han introduït en ordre creixent.
#!/bin/bash
    # Ask the user for their name
echo Pon tres numeros en orden creciente
echo Primer Numero
    read varname
num1=$varname
echo segundo Numero
    read varname
num2=$varname
echo tercer Numero
read varname
num3=$varname
re='^[0-9]+$'
if ! [[ $num1 =~ $re ]] ; then
   echo "error, $num1 is not a number" >&2; exit 1
fi
if ! [[ $num2 =~ $re ]] ; then
   echo "error, $num2 is not a number" >&2; exit 1
fi
if ! [[ $num3 =~ $re ]] ; then
   echo "error, $num3 is not a number" >&2; exit 1
fi
if [ $num1 -gt $num2 ]; then
   echo "$num1 es mas grande que $num2" >&2; exit 1
   fi
if [ $num2 -gt $num3 ]; then
   echo "$num2 es mas grande que $num3" >&2; exit 1
    fi
echo "----------------"
echo "Primero| $num1"
echo "----------------"
echo "Segundo| $num2"
echo "----------------"
echo "Tercero | $num3"
echo "----------------"