# Fer un script que rebi un número i mostri en pantalla el mateix nombre de asteriscs.
#!/bin/sh
e=$1
i=0
while [ $i -lt $e ]
do
    echo -n "*"
    i=$[$i+1]
done

