# Fer un script que rebi  dos números i mostri tots els números que van des del primer al segon. S'ha de controlar que els valors són correctes.
#!/bin/sh
num1=$1
num2=$2
re='^[0-9]+$'
if ! [[ $num1 =~ $re ]] ; then
   echo "error: Not a number" >&2; exit 1
fi
if ! [[ $num2 =~ $re ]] ; then
   echo "error: Not a number" >&2; exit 1
fi
if [[ $num2 = $num1 ]] ; then
   echo "error: same number" >&2; exit 1
fi
if [ $1 \> $2 ];
    then
        num2=$1
        num1=$2
    else
        num1=$1
        num2=$2
    fi
#echo $num1
#echo $num2
for (( c=$num1; c<=$num2; c++ ))
do
    echo "$c"
done
