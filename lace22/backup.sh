#!/bin/bash
FILE=~/backups
if [ -d "$FILE" ]; then
    echo "$FILE already exists, skipping..."
    else
    echo "$FILE does not exist"
    sleep 1
    echo "Creating $FILE directory"
    mkdir $FILE
    sleep 1
    echo "$FILE directory created"
fi
d=`date +%Y_%m_%d`
cd ~/
echo "Backing Up"

tar --exclude=backups -Pczf ~/backups/copia_${d}.tar.gz ~/
#tar -Pcf --exclude=~/backups - ~/ | pv -s $(du -sb ~/ | awk '{print $1}') | gzip > ~/backups/copia_${d}.tar.gz
echo -"copia_${d}.tar.gz,$d" >> ~/.backups/log
echo "Checking if there is an older than 15 days backup"
find ~/backups -type f -mtime +15 -name '*tar.gz' -execdir rm -- '{}' \;
