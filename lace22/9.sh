#!/bin/sh
# 09) Fer un script que mostri els números de l'1 al 100 en una taula de 10x10.
e=0
i=1
while [ $i -lt 101 ]
do
    if [ $i -gt 9 ] 
    then
        echo -n "|$i|"
        i=$[$i+1]
        e=$[$e+1]
    else    
        echo -n "|0$i|"
        i=$[$i+1]
        e=$[$e+1]

    fi 
        if [ $e -eq 10 ]
        then
            echo " "
            e=0
            fi
    
done
echo -n