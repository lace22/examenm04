#!/bin/sh
ST1=0
ST2=0
if [ "$#" != "2" ]
then
echo "No hay tres argumentos";
clear

case $1 in
    ''|*[!0-9]*) let ST1=0 ;;
    *) let ST1=1 ;;
esac
case $2 in
    ''|*[!0-9]*) let ST2=0 ;;
    *) let ST2=1 ;;
esac;fi

if [ $ST1 = $ST2 ]
then
    if [ "$1" -gt "$2" ]
        then
        echo "$1 > $2"
        fi
    if [ "$2" -gt "$1" ]
        then
        echo "$1 < $2"
        fi
    if [ "$1" == "$2" ]
        then
        echo "Son iguales"
    fi    
    
else
    echo "No son numeros";    
fi