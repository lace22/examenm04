if [ "$#" != "2" ]
then
echo "No hay dos argumentos";
fi

str1="$1"
str2="$2"
length=${#str1}
length2=${#str2}

if [ $length -gt $length2 ]
then
echo "$1 ($length) tiene mas letras que $2 ($length2)"
else
echo "$2 ($length2) tiene mas letras que $1 ($length)"
fi