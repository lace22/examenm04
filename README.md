# examenm04
# Haceos una carpeta para vuestros scripts UWU y haced git pull antes de un commit si no explota

## Comprobar que un directorio existe

```bash
if [ -d "/path/to/dir" ] 
then
    echo "Directory /path/to/dir exists." 
else
    echo "Error: Directory /path/to/dir does not exists."
fi

```
## Comprobar que un archivo existe
```bash
FILE=/etc/resolv.conf
if test -f "$FILE"; then
    echo "$FILE exist"
fi
```
# Comprobar si es un numero
```bash
while ! [[ "$voltes" =~ ^[0-9]+$ ]];
do
    read -p "VOltes" voltes
done

```
## Excluir números

```bash
![0-9]
if ! [[ "variableX" =~ ^[0-9]+$ ]];
    then
            echo "solo números"
fi
```
## Contar dígitos
```bash
echo "${#1}"
```
# COSAS CON TAR


## To extract a .gz archive:
```bash
tar -xzvf /path/to/foo.tgz
```
## To create a .gz archive:
```bash
tar -czvf /path/to/foo.tgz /path/to/foo/
```

## To extract a .tar in specified Directory:
```bash
tar -xvf /path/to/foo.tar -C /path/to/destination/
```

## To create a .gz archive and exclude all jpg,gif,... from the tgz
```bash
tar czvf /path/to/foo.tgz --exclude=\*.{jpg,gif,png,wmv,flv,tar.gz,zip} /path/to/foo/
```

# Leer CSVs en una Array
```bash
#!/bin/bash

N=0
ARR=()

IFS=","

while read STR
do
        set -- "$STR"

        while [ "$#" -gt 0 ]
        do
                ARR[$N]="$1"
                ((N++))
                shift
        done
done < input.csv
```
