#!/bin/bash

clear

CUENTA=1
NUMERO=1

while test $CUENTA -le 50
do
  DIVISION=`expr $NUMERO % 2`

  if test $DIVISION -ne 0
  then
      echo "$NUMERO"
      CUENTA=`expr $CUENTA + 1`
  fi

  NUMERO=`expr $NUMERO + 1`
done
