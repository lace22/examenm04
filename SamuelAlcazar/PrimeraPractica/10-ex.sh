#!/bin/bash

clear

tabla=0
multiplicador=0

echo "====================="
echo "Tablas de multiplicar"
echo "====================="

while true
do
  echo "$tabla x $multiplicador = $((tabla*multiplicador))"
  multiplicador=$(($multiplicador+1))
  echo " "

  if [[ $multiplicador -eq 11 ]] && [[ $tabla -eq 10 ]]
  then
    break

  elif [[ $multiplicador -eq 11 ]]
  then
    tabla=$(($tabla+1))
    multiplicador=0
    echo "Tabla $tabla"
    echo "------------------"
  fi
done
echo " "
