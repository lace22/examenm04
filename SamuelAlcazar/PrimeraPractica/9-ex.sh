#!/bin/bash

clear

cuenta=0

for numero in {01..100}
do
  if [[ $(($cuenta%10)) -eq 0 ]]
  then
    echo -e "\n"
    echo -e "$numero \c"
    cuenta=$(($cuenta+1))
  else
    echo -e "$numero \c"
    cuenta=$(($cuenta+1))
  fi
done
echo -e "\n"
