#!/bin/bash

clear

# Creamos las variables
echo "Solo son validos numeros enteros y positivos."
echo ""
echo "Pon numeros altos, por los jajas"
echo ""
# Comprobacion de numero

ACEPTADOS='^[0-9]+'  # Esto significa todos los numeros que sean enteros y positivos.

read -p "Introduce un numero entero positivo: " NUM1

if ! [[ $NUM1 =~ $ACEPTADOS ]] ; then # =~ si la cadena esta dentro de la lista
   echo "ERROR: $NUM1 No es un número"
   exit
fi

read -p "Introduce otro numero: " NUM2

if ! [[ $NUM2 =~ $ACEPTADOS ]] ; then
  echo "ERROR: $NUM2 No es un número"
  exit
fi

# Script

if [ $NUM1 -lt $NUM2 ] ; then
  while [ $NUM1 -le $NUM2 ]
  do
    echo "$NUM1"
    NUM1=$(($NUM1+1))
    sleep 0.0001
  done
elif [ $NUM2 -lt $NUM1 ] ; then
  while [ $NUM2 -le $NUM1 ]
  do
    echo "$NUM2"
    NUM2=$(($NUM2+1))
    sleep 0.0001
  done
fi
