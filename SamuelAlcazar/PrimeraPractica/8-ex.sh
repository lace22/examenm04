#!/bin/bash

clear

# Variables

echo ""
read -p "Escribe un numero: " NUM1
echo ""

if ! [[ $NUM1 =~ $ACEPTADOS ]] ; then
   echo "ERROR: $NUM1 No es un número"
   exit
fi

CUENTA=1

# Validación

echo "*"
while [ $CUENTA -lt $NUM1 ]
do
  echo "*"
  CUENTA=$(($CUENTA+1))
done
