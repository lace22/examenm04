#!/bin/bash

clear

echo "========================="
echo "=Ordena de mayor a menor="
echo "========================="

read -p "Escribe un numero: " num1
read -p "Escribe otro numero: " num2
read -p "Venga va, uno más: " num3

if [[ $num1 -gt $num2 ]]
then
  if [[ $num2 -gt $num3 ]]
  then
    echo "$num1"
    echo "$num2"
    echo "$num3"
  else
    echo "$num1"
    echo "$num3"
    echo "$num2"
  fi
fi

if [[ $num2 -gt $num1 && $num3 -lt $num1 ]]
then
  if [[ $num1 -gt $num3 ]]
  then
    echo "$num2"
    echo "$num1"
    echo "$num3"
  else
    echo "$num2"
    echo "$num3"
    echo "$num1"
  fi
fi

# No tocar por encima

if [[ $num3 -gt $num2 ]]
then
  if [[ $num2 -gt $num1 ]]
  then
    echo "$num3"
    echo "$num2"
    echo "$num1"
  else
    echo "$num3"
    echo "$num1"
    echo "$num2"
  fi
fi
