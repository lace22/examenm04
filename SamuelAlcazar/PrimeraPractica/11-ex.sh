#!/bin/bash
clear

multiplicador=0

echo "======================="
echo "=Tablas de multiplicar="
echo "======================="

read -p "¿Que tabla quieres multiplicar? " tabla

while true
do
  echo "$tabla x $multiplicador = $((tabla*multiplicador))"
  multiplicador=$((multiplicador+1))

  if [[ $multiplicador -eq 11 ]]
  then
    break
  fi
done
