#!/bin/bash

aceptados='^[0-9]+([.][0-9]+)?$'

cero='0'
while true
do
	clear

	echo "============"
	echo "|  Reglas  |"
	echo "============"
	echo "	- Numeros negativos no aceptados."
	echo "	- Numero 0 no aceptado."
	echo "	- El dado puede ser como mucho 32767 caras."
	echo " "
	echo " "
	read -p  "- Escribe un número de caras valido ====> " numero

	if ! [[ $numero =~ $aceptados ]]
	then
		echo "El numero no es valido."
		sleep 3
		continue
	elif [[ $numero -gt 32767 ]]
	then
		echo "El numero no es valido."
		sleep 3
		continue
	elif [[ $numero = $cero  ]]
	then
		echo "El numero no es valido."
		sleep 3
		continue
	else
		break
	fi

done
echo ""
echo "La cara del dado es: "
echo "--------------------"
echo $(($RANDOM % $numero + 1))
