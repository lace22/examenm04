#!/bin/bash
declare -a array
i=0
while [ $i -le 9 ]
do
	read -p "POSA UN NUMERO :" num
	if ! [[ "$num" =~ ^[0-9]+$ ]];
	then
	echo "-------------------------------------------------"
	echo "NO ES PODEN POSAR CARÀCTERS QUE NO SIGUIN NÚMEROS"
	echo "-------------------------------------------------"
else
array[i]=$num
((i++))
fi
done
echo ${array[@]} | tr " " "\n" > archivo
sort -nr archivo >> archivo2
primer=$(sed '1!d' archivo2)
ultim=$(sed '10!d' archivo2)
echo "-------------------------------"
echo "El major número és: $primer"
echo "El menor número és: $ultim"
echo "-------------------------------"
rm -f archivo
rm -f archivo2
