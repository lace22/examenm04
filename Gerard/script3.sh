#!/bin/bash
if [ $# -eq 2 ];
then
	echo $1 $2 		
	if [ $1 -gt $2 ];
	then 
		echo "El $1 és major que $2"
	else
		echo "El $2 és major que $1"
	fi
else
		echo "Arguments invàlids, has introduit $# arguments"
fi
